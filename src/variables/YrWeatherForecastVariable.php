<?php
/**
 * YR weather forecast plugin for Craft CMS 3.x
 *
 * Craft 3 plugin that fetches weather forcast
 *
 * @link      https://jerryogconrad.no/
 * @copyright Copyright (c) 2018 Johan Graucob
 */

namespace jerryogconrad\yrweatherforecast\variables;

use jerryogconrad\yrweatherforecast\YrWeatherForecast;

use Craft;

/**
 * YR weather forecast Variable
 *
 * Craft allows plugins to provide their own template variables, accessible from
 * the {{ craft }} global variable (e.g. {{ craft.yrWeatherForecast }}).
 *
 * https://craftcms.com/docs/plugins/variables
 *
 * @author    Johan Graucob
 * @package   YrWeatherForecast
 * @since     1.0.0
 */
class YrWeatherForecastVariable
{
    // Public Methods
    // =========================================================================

    /**
     * Whatever you want to output to a Twig template can go into a Variable method.
     * You can have as many variable functions as you want.  From any Twig template,
     * call it like this:
     *
     *     {{ craft.yrWeatherForecast.exampleVariable }}
     *
     * Or, if your variable requires parameters from Twig:
     *
     *     {{ craft.yrWeatherForecast.exampleVariable(twigValue) }}
     *
     * @param null $optional
     * @return string
     */
    public function exampleVariable($optional = null)
    {
        $result = "And away we go to the Twig template...";
        if ($optional) {
            $result = "I'm feeling optional today...";
        }
        return $result;
    }

    public function getForecast(){
        return YrWeatherForeCast::$plugin->forecast->fetchForecast();
    }
}
