<?php
/**
 * YR weather forecast plugin for Craft CMS 3.x
 *
 * Craft 3 plugin that fetches weather forcast
 *
 * @link      https://jerryogconrad.no/
 * @copyright Copyright (c) 2018 Johan Graucob
 */

/**
 * YR weather forecast en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('yr-weather-forecast', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Johan Graucob
 * @package   YrWeatherForecast
 * @since     1.0.0
 */
return [
    'YR weather forecast plugin loaded' => 'YR weather forecast plugin loaded',
];
