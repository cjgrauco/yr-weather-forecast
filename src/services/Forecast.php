<?php
/**
 * YR weather forecast plugin for Craft CMS 3.x
 *
 * Craft 3 plugin that fetches weather forcast
 *
 * @link      https://jerryogconrad.no/
 * @copyright Copyright (c) 2018 Johan Graucob
 */

namespace jerryogconrad\yrweatherforecast\services;



use Craft;
use craft\base\Component;
use GuzzleHttp\Client;
use \GuzzleHttp\json_decode;
use \GuzzleHttp\json_encode;

/**
 * Forecast Service
 *
 * All of your plugin’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Johan Graucob
 * @package   YrWeatherForecast
 * @since     1.0.0
 */
class Forecast extends Component
{

    protected $allowAnonymous = true;
    private $fnugg_url;
    // Public Methods
    // =========================================================================

    /**
     * This function can literally be anything you want, and you can have as many service
     * functions as you want
     *
     * From any other plugin file, call it like this:
     *
     *     YrWeatherForecast::$plugin->forecast->exampleService()
     *
     * @return mixed
     */

    public function __construct()
    {
        parent::__construct();
        $this->fnugg_url = 'https://api.fnugg.no/search?q=stryn%20sommerski';
}

    public function fetchForecast()
    {
        $result = [];

        $obj = $this->curlCall();


        if($obj === null){
            $result['longterm']['forecast'] = null;
            $result['today']['forecast'] = null;
        }else{
            $conditions = $obj['hits']['hits'][0]['_source']['conditions'];
            $result['longterm'] = $obj['hits']['hits'][0]['_source']['conditions']['forecast']['long_term'];
            $result['today'] = $obj['hits']['hits'][0]['_source']['conditions']['forecast']['today'];
        }

        return $result;
    }


    // Private functions
    private function curlCall()
    {
        $response = null;
        $result = null;

        $session = curl_init();

        curl_setopt($session, CURLOPT_URL, $this->fnugg_url);
        curl_setopt($session,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($session,CURLOPT_HEADER, false);
        curl_setopt($session, CURLOPT_CONNECTTIMEOUT, 5);

        try {
            $response = curl_exec($session);
        } catch (\Exception $e) {
            Craft::error('Fnug curl-call failed: ' . curl_error($session) . '| Exception: ' . $e);
        }

        curl_close($session);

        $decoded = json_decode($response, true);

        if(array_key_exists('forecast', $decoded['hits']['hits'][0]['_source']['conditions'])){
            $result['longterm'] = $decoded['hits']['hits'][0]['_source']['conditions']['forecast']['long_term'];
            $result['today'] = $response['hits']['hits'][0]['_source']['conditions']['forecast']['today'];
        }



        return $result;
    }

    private function guzzleCall()
    {
        $result = [];
        $client = new Client();

        try {
            $res = $client->get('https://api.fnugg.no/search?q=stryn%20sommerski',[
                'timeout' => 5
                ]);
            $content = $res->getBody()->getContents();
        } catch (\Exception $e) {
            Craft::error($e);
            throw $e;
        }


        $decoded = json_decode($content, true);

        $result['longterm'] = $decoded['hits']['hits'][0]['_source']['conditions']['forecast'];
        $result['today'] = $decoded['hits']['hits'][0]['_source']['conditions']['forecast']['today'];


        return $result;
    }

}
