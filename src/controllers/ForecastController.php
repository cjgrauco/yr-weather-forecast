<?php
/**
 * YR weather forecast plugin for Craft CMS 3.x
 *
 * Craft 3 plugin that fetches weather forcast
 *
 * @link      https://jerryogconrad.no/
 * @copyright Copyright (c) 2018 Johan Graucob
 */

namespace jerryogconrad\yrweatherforecast\controllers;

use jerryogconrad\yrweatherforecast\YrWeatherForecast;


use Craft;
use craft\web\Controller;

/**
 * Forecast Controller
 *
 * Generally speaking, controllers are the middlemen between the front end of
 * the CP/website and your plugin’s services. They contain action methods which
 * handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering
 * post data, saving it on a model, passing the model off to a service, and then
 * responding to the request appropriately depending on the service method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what
 * the method does (for example, actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 *
 * @author    Johan Graucob
 * @package   YrWeatherForecast
 * @since     1.0.0
 */
class ForecastController extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = ['index', 'do-something', 'fetch-forecast'];

    // Public Methods
    // =========================================================================

    /**
     * Handle a request going to our plugin's index action URL,
     * e.g.: actions/yr-weather-forecast/forecast
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $result = 'Welcome to the ForecastController actionIndex() method';

        return $result;
    }

    /**
     * Handle a request going to our plugin's actionDoSomething URL,
     * e.g.: actions/yr-weather-forecast/forecast/do-something
     *
     * @return mixed
     */
    public function actionDoSomething()
    {
        $result = 'Welcome to the ForecastController actionDoSomething() method';

        return $result;
    }

    public function actionFetchForecast(){
            return YrWeatherForecast::$plugin->forecast->fetchForecast();

    }
}
